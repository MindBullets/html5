window.onload = function(){

const filled = document.querySelectorAll(".fill");
const emptyBoxes = document.querySelectorAll(".empty");

//drag
function dragStart(ev) {
    console.log("start");
    // ev.dataTransfer.effectAllowed = "move";          
    ev.dataTransfer.setData( "text", ev.target.id );
    // setTimeout(() => this.className = 'invisible', 0);
    
}

function dragEnd() {
    console.log("end");
    this.className = "fill";
}

function dragOver(ev) {
    ev.preventDefault();
    console.log("over");
}

function dragEnter(ev) {
    console.log("enter");
    ev.preventDefault();
    this.className += " hovered";
}

function dragLeave() {
    console.log("leave");
    this.className = "empty";
}

function drop(ev) {
    if (!this.hasChildNodes()) {
        this.className = "empty";
        ev.preventDefault();
        var data = ev.dataTransfer.getData("text");
        ev.target.appendChild( document.getElementById(data) );    
        console.log("drop");
    }
    console.log("nope");
    ev.preventDefault();
    
}


//fill listener
for (const fill of filled) {
    fill.addEventListener("dragstart", dragStart);
    fill.addEventListener("dragend", dragEnd);
    
}

//emptyBoxes listener
for (const empty of emptyBoxes) {
    empty.addEventListener( "dragover", dragOver);
    empty.addEventListener( "dragenter", dragEnter);
    empty.addEventListener( 'dragleave', dragLeave);
    empty.addEventListener( "drop", drop);
}

}