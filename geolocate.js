// globals
var map;
var watchId;
//mt fuji
var ourCoords = {
    latitude: 35.360554,
    longitude: 138.727783
};
// watchId = navigator.geolocation.watchPosition(displayLocation, displayError, options);


var options = {
    enableHighAccuracy: true,
    timeout: 300000,
    maximumAge: 1000
};

function clearWatchMe() {
	// alert('not watching');

	if(watchId !== undefined) {
		watchId = navigator.geolocation.clearWatch(watchId);
		// watchId = null;
		alert('not watching');


	}
}

function watchLocation(){
	watchId = navigator.geolocation.watchPosition(displayLocation, displayError, options);
	// alert('watching');
	alert("Geolocation active");
}

function getMyLocation() {
	var watchButton = document.getElementById("watch");
	watchButton.onclick = watchLocation;
	var clearWatchButton = document.getElementById("clear");
	clearWatchButton.onclick = clearWatchMe;

    if(navigator.geolocation) {
        alert("Get my location");
		 navigator.geolocation.watchPosition(displayLocation, displayError, options);
    } else {
        alert("Sorry, there is no geolocation support");
    }
}
window.onload = getMyLocation;

function displayLocation(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;
    var myLocation = document.getElementById("location");
    myLocation.innerHTML = "You are at Latitude: "+latitude+ ", Longitude: "+longitude;
    myLocation.innerHTML += " (with " + position.coords.accuracy+ " meters accuracy)";

    var km = computeDistance(position.coords, ourCoords);
    var distance = document.getElementById("distance");
    distance.innerHTML = "You are ~" +Math.round(km) + " km from Mt. Fuji";
    showMap(position.coords);
}

function displayError(error) {
    switch (error.code) {
        case error.PERMISSION_DENIED:
            alert('Geolocation Permission denied');
            break;
        case error.POSITION_UNAVAILABLE:
            alert('Geolocation Position unavailable');
            break;
        case error.TIMEOUT:
            alert('Geolocation request timed out');
            break;
        default:
            alert('Geolocation returned an unknown error: ' + error.code);
            break;
    }
}

// Calculating Distance:

function computeDistance(startCoords, destCoords) {
    var startLatRads = degreesToRadians(startCoords.latitude);
    var startLongRads = degreesToRadians(startCoords.longitude);
    var destLatRads = degreesToRadians(destCoords.latitude);
    var destLongRads = degreesToRadians(destCoords.longitude);

    var Radius = 6371; // radius of the earth in km
    var distance = Math.acos(Math.sin(startLatRads) * Math.sin(destLatRads) + Math.cos(startLatRads) * Math.cos(destLatRads) * Math.cos(startLongRads - destLongRads)) * Radius;
    return distance;

}

function degreesToRadians(degrees) {
    var radians = (degrees * Math.PI) / 180;
    return radians;

}

function showMap(coords) {
    var googleLatAndLong = new google.maps.LatLng(coords.latitude, coords.longitude);

    var mapOptions = {
        zoom: 10,
        center: googleLatAndLong,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var mapDiv = document.getElementById("map");
    map = new google.maps.Map(mapDiv, mapOptions);

    // add marker
    var title = "Your Location";
    var content = "Your are here " + coords.latitude + " , " + coords.longitude;
    addMarker(map, googleLatAndLong, title, content);
}

function addMarker(map, latlong, title, content) {
    var markerOptions = {
        position: latlong,
        map: map,
        title: title,
        clickable: true
    };

    var marker = new google.maps.Marker(markerOptions);

    var infoWindowOptions = {
        content: content,
        position: latlong
    };

    var infoWindow = new google.maps.InfoWindow(infoWindowOptions);

    google.maps.event.addListener(marker, "click", function() {
        infoWindow.open(map);
    });
}
