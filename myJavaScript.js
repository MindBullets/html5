$(function () {
    $('.contact-toggle').on('click', function () {
        if ($('#itinerary-flyout').hasClass('open')) {
            $('#itinerary-flyout').toggleClass('open');
        }
        $('#right-flyout').toggleClass('open');
        $('#fullname').focus();

    });

});

$(function () {
    $('.itinerary-toggle').on('click', function () {
        if ($('#right-flyout').hasClass('open')) {
            $('#right-flyout').toggleClass('open');
        }
        $('#itinerary-flyout').toggleClass('open');
    });
});

window.onload = function(){
    // var canvas = document.querySelector('canvas');
    var cnv = document.getElementById('culture-canvas');
    cnv.width = window.innerWidth;
    cnv.height = 600;

    var ctx = cnv.getContext('2d');
    
    // draw an X
    ctx.strokeStyle = 'red';     
    ctx.lineWidth = '2'; 

    ctx.moveTo(48,32);      
    ctx.lineTo(64,48);     
    ctx.stroke();

    ctx.moveTo(64,32);      
    ctx.lineTo(48,48);     
    ctx.stroke();

    //draw O
    ctx.beginPath();
    ctx.strokeStyle = 'green';
    ctx.arc(56,73,9,0,Math.PI*2,false);
    ctx.stroke();     

    //draw triangle
    ctx.beginPath();
    ctx.moveTo(56, 95); 
    ctx.lineTo(65, 110); 
    ctx.lineTo(47, 110); 
    ctx.closePath();

    // Draw the outline. 
    ctx.lineWidth = 2; 
    ctx.strokeStyle = "orange"; 
    ctx.stroke();

    // draw square
    ctx.beginPath();
    ctx.strokeStyle = "black"; 
    ctx.strokeRect(48, 128, 17, 17); 

    // draw text
    ctx.font = '18px "Arial"';
    ctx.fillText('X or “batsu” means incorrect. Wrong answer on tests are marked with an X', 88, 46);
    ctx.fillText('O or “maru” means correct. Right answers are marked with an O', 88, 79);
    ctx.fillText('Triangle means almost correct. Answers that are close are marked with this', 88, 109);
    ctx.fillText('All the above symbols and square make up the Playstation Logo', 88, 143);

    //draw images
    var pict = new Image();       
    pict.src = "images/badtzmaru.jpg";    
    pict.onload = function() {          
          ctx.drawImage(pict, 47, 162);       
    }        
    
    //image caption
    ctx.fillText('Badtzmaru is a popular Sanrio character who’s name literally translates as wrong-correct', 47, 508);


     // var canvas = document.querySelector('canvas');
     var aniCnv = document.getElementById('animation-canvas');
     aniCnv.width = window.innerWidth;
     aniCnv.height = 600;
 
    var aniCtx = aniCnv.getContext('2d');


    /*
    x = initial x pos
    y = initial y pos
    r = radius
    dx = x velocity
    dy = y velocity
    */
    function Circle( x, y, r, dx, dy ) {
        this.x = x;
        this.y = y;
        this.r = r;
        this.dx = dx;
        this.dy = dy;

        this.draw = function() {
            aniCtx.beginPath();
            aniCtx.strokeStyle = 'green';
            aniCtx.arc(this.x, this.y, this.r, 0, Math.PI*2, false);
            aniCtx.stroke();
        }

        this.update = function () {
            if ( ( this.x + this.r ) > aniCnv.width || ( this.x - this.r ) < 0 ) {
                this.dx = -this.dx;
            }
    
            if ( ( this.y + this.r ) > aniCnv.height || ( this.y - this.r ) < 0 ) {
                this.dy = -this.dy;
            }
    
            this.x += this.dx;
            this.y += this.dy;

            this.draw();
        }
    }


   
    // myCircle vars 
    var r = 9;
    var x = Math.random() * ( aniCnv.width - r );
    var y = Math.random() * ( aniCnv.height - r );
    var dx = 4;
    var dy = 4;
    var maxCircles = 100;

    var circleArr = [];
    for (let i = 0; i < maxCircles; i++) {
        var r = Math.random() * 30;
        var x = Math.random() * ( aniCnv.width - ( r * 2 ) ) + r;
        var y = Math.random() * ( aniCnv.height - ( r * 2 ) ) + r;
        var dx = Math.random() * 2;
        var dy = Math.random() * 2;
        circleArr.push(new Circle(x, y, r, dx, dy ));
        
    }

    function animate() {
        aniCtx.clearRect( 0, 0, innerWidth, innerHeight );
        for (let i = 0; i < circleArr.length; i++) {
            circleArr[i].update();            
        }
    }
    
    setInterval(animate, 10);   
}

